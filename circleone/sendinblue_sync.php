<?php 
// Check and sync Customer into Sendinblue.
require_once("lib/common.php");
require_once(DIR_WS_ADMIN_INCLUDES . "functions.php");
require_once(DIR_WS_MODEL . 'UserMaster.php');

require_once(DIR_WS_EXTERNALSERVICE."sendinblue/model/SendinblueMaster.php");
require_once(DIR_WS_EXTERNALSERVICE."sendinblue/model/SendinblueSyncLogMaster.php");
require_once(DIR_WS_EXTERNALSERVICE."sendinblue/sendinblue.php");
require_once(DIR_WS_MODEL . "ExternalServiceMaster.php");
require_once(DIR_WS_MODEL . 'orderHistoryMaster.php');
require_once(DIR_WS_MODEL . "CountryMaster.php");
require_once(DIR_WS_MODEL . "SubscriberMaster.php");

$objUserMaster = new UserMaster();
$objSendinblueData = new SendinblueData();
$objSendinblueMaster = new SendinblueMaster();
$objSendinblueSyncLogMaster = new SendinblueSyncLogMaster();
$objSendinblueSyncLogData = new SendinblueSyncLogData();
$objExternalServiceMaster = new ExternalServiceMaster();
$objExternalServiceData = new ExternalServiceData();
$objCountryMaster = new CountryMaster();
$objSubscriberMaster = new SubscriberMaster();

$userIds = initRequestValue('userid', '');
$operation = initRequestValue('type', 'add');
$userType = initRequestValue('user', 'webuser'); // Available option for this all,webuser,newsletter

/**Newsletter :: START */
$syncFromTo = initRequestValue('start'); // Start number sync.
$syncFrom = initRequestValue('start'); // Start number sync.
$syncTo = initRequestValue('end', 100); // End Number for Sync.
$limit = initRequestValue('limit'); // Total Number of sync data.
/**Newsletter :: END */

$objExternalServiceMaster->setWhere("AND directory = :directory ", 'sendinblue', "string");
$configData = $objExternalServiceMaster->getExternalService();
$configData = $configData[0];
$serviceId = $configData['service_id'];
$unserializedData = unserialize($configData['service_details']);

$apiKey = $unserializedData['apiKey'];
$countryCode = $unserializedData['countrycode'];
$listId = (int) $unserializedData['list_id']; // List Id.
$autoSync = $unserializedData['autoStart'];
$lastSyncDate = $unserializedData['last_sync_cron'];
$lastSyncDateModify = $unserializedData['last_sync_cron_modify'];

/**Newsletter :: Subscriber START */
$lastSubscriberSync = $unserializedData['last_subscriber_sync_cron'];
$lastSubscriberSyncModify = $unserializedData['last_subscriber_sync_cron_modify'];

$websyncFromTo = $unserializedData['webSyncFrom']; // Sync From For webuser. 
if(!isset($syncFrom)  && $userType == 'webuser') {
    $syncFrom = $websyncFromTo;
}
$syncFromTo = $unserializedData['sibSyncFrom']; // Sync From. 
if(!isset($syncFrom) && $userType == 'newsletter') {
    $syncFrom = $syncFromTo;
}
/**Newsletter :: END */

/* Admin Constant Start */
require_once(DIR_WS_MODEL . "AdminConstantMaster.php");
$objAdminConstantMaster = new AdminConstantMaster();
$constStr = array('EXTERNAL_SERVICE_SENDINBLUE_ACCOUNTAPI_SYNC_ERROR', 'EXTERNAL_SERVICE_ACCOUNTAPI_SYNC_SUCCESS', 'COMMON_CLICK_HERE', 'COMMON_RETAILER', 'USER_GUEST', 'COMMON_CORPORATE');
$arrField = array('admin_constants.constant_name', 'admin_constants_description.constant_value');
$objAdminConstantMaster->setSelect($arrField);
$objAdminConstantMaster->setWhere("AND admin_constants.constant_name IN @const_str ", $constStr, "string");
$adminDataConstants = $objAdminConstantMaster->getAdminConstantDetails(SITE_LANGUAGE_ID);
foreach($adminDataConstants as $constants) {
    if(!empty($constants['constant_name'])) {
        define($constants['constant_name'], $constants['constant_value']);
    }
}
/* Admin Constant END */

/* Get Country code */
$objCountryMaster->setWhere("AND status = :country_status", '1', 'string');
$countryData = $objCountryMaster->getCountry();
$countriesISD = array();
foreach($countryData as $cKey => $cvalue) {
    if(!empty($cvalue['isd_code'])) {
        $countriesISD[$cvalue['countries_id']]['isd_code'] = $cvalue['isd_code'];
        $countriesISD[$cvalue['countries_id']]['country_name'] = $cvalue['countries_name'];
    }
}

// Get Synchronize users ids for cron file.
 if($userType == 'newsletter') {
    $objSendinblueMaster->setWhere("sendinblue_sync_data.type = :crontype", 'S', 'string');
} else {
    $objSendinblueMaster->setWhere("sendinblue_sync_data.type = :crontype", 'C', 'string');
}
$orderIdDatas = $objSendinblueMaster->getSendinblueSyncDetail();
$synchronizedUserIds = array();
foreach($orderIdDatas as $value) {
    $synchronizedUserIds[] = $value['type_id'];
}
$cronAction = true;
if($userType == 'newsletter') {
    $objSendinblueMaster->setWhere("AND subscriber.status = :start_status", 1, "string");
    if(!isset($limit)) {
        $objSendinblueMaster->setWhere("AND subscriber.created_on >= :start_register_date", $lastSubscriberSync, "string", true);
    }
    if(!empty($synchronizedUserIds) && $operation != 'update') {
        $objSendinblueMaster->setWhere("AND subscriber.id NOT IN @ids", $synchronizedUserIds, "int");
    }
} else {
    $objSendinblueMaster->setWhere("AND user_master.status = :start_status", 1, "string");
    if(!isset($limit)) {
        if($registerDate == 'register') {
            $objSendinblueMaster->setWhere("AND user_master.register_date >= :start_register_date", $lastSyncDate, "string");
        } elseif($registerDate == 'modify') {
            $objSendinblueMaster->setWhere("AND user_master.modified_on >= :start_modified_on", $lastSyncDateModify, "string");
        }
    }
    if(!empty($synchronizedUserIds) && $operation != 'update') {
        $objSendinblueMaster->setWhere("AND user_master.userid NOT IN @userids", $synchronizedUserIds, "int");
    }
}
if(!empty($userIds) || ($userType == 'webuser')) {
    $fieldArr = array("IF(sendinblue_sync_data.type='C',sendinblue_sync_data.sendinblue_type_id,'') as senduid", "sendinblue_sync_data.*");
    $objSendinblueMaster->setSelect($fieldArr);
    $objSendinblueMaster->setSelect("user_master.*", "corporate_user.fullname", "corporate_user.email as corporate_email", "user_login_details.last_login_date");
    $objSendinblueMaster->setSelect("address_book.street_address, address_book.suburb, address_book.postcode, address_book.city, address_book.state, address_book.country");
    $objSendinblueMaster->setSelect("IF (corporate_user.corporate_name IS NULL , IF(user_master.customer_type = '0' ,:user_customer_type , :user_customer_type2) , corporate_user.corporate_name) AS corporate_name", array(':user_customer_type' => USER_GUEST, ':user_customer_type2' => COMMON_RETAILER), array(':user_customer_type' => 'string', ':user_customer_type2' => 'string'));
    $objSendinblueMaster->setSelect("IF(user_master.user_type_id = '2' ,'" . COMMON_CORPORATE . "' , '" . COMMON_RETAILER . "') AS customer_type");
    $objSendinblueMaster->setJoin("LEFT JOIN address_book ON user_master.userid = address_book.user_id AND address_book.from_signup = '1' ");
    $objSendinblueMaster->setJoin("LEFT JOIN user_login_details ON user_master.userid = user_login_details.userid");
    $objSendinblueMaster->setJoin("LEFT JOIN countries ON countries.countries_id = user_master.country");
    $objSendinblueMaster->setJoin("LEFT JOIN corporate_user ON corporate_user.corporate_id = user_master.corporate_id");
    $objSendinblueMaster->setJoin("LEFT JOIN sendinblue_sync_data ON (user_master.userid = sendinblue_sync_data.type_id AND sendinblue_sync_data.type = :type)", array(":type" => "C"), array(":type" => "string"));
    if($cronAction == true) {
        $objSendinblueMaster->setLimit($syncFrom, $limit);
    }
    $objSendinblueMaster->setOrderBy("user_master.userid ASC");
    $syncUserData = $objSendinblueMaster->getSendinblueUserDetail(); // Fetch
} else {
    $fieldArr = array("IF(sendinblue_sync_data.type='S',sendinblue_sync_data.sendinblue_type_id,'') as subscriberuid", "subscriber.id as userid", "subscriber.firstname", "subscriber.lastname", "subscriber.email_address as email", "subscriber.created_on as register_date", "sendinblue_sync_data.*");
    $objSendinblueMaster->setSelect($fieldArr);
    $objSendinblueMaster->setWhere("AND subscriber.email_address NOT IN (SELECT email from user_master)");
    $objSendinblueMaster->setJoin("LEFT JOIN sendinblue_sync_data ON (subscriber.id = sendinblue_sync_data.type_id AND sendinblue_sync_data.type = :type)", array(":type" => "S"), array(":type" => "string"));
    if($cronAction == true) {
        $objSendinblueMaster->setLimit($syncFrom, $limit);
    }
    $objSendinblueMaster->setOrderBy("subscriber.id ASC");
    $syncUserData = $objSendinblueMaster->getSendinblueUserDetail('subscriber'); // Fetch Data
}

// Get bind customers
$fieldArr = array();
$fieldArr = array("sendinblue_type_id", "type_id");
$objSendinblueMaster->setSelect($fieldArr);
$objSendinblueMaster->setWhere("AND type = :type", 'C', 'string');
$dataCustomer = $objSendinblueMaster->getSendinblueSyncDetail(); // Fetch Data
$syncCustomerIds = array();
foreach($dataCustomer as $val) {
    $syncCustomerIds[] = $val['type_id'];
    $syncCustomerSendinblueIds[$val['type_id']] = $val['sendinblue_type_id'];
}

$objSendinblueMaster->setWhere("AND type = :type", 'S', 'string');
$dataCustomer = $objSendinblueMaster->getSendinblueSyncDetail(); // Fetch Data
$syncSubscriberIds = array();
foreach($dataCustomer as $val) {
    $syncSubscriberIds[] = $val['type_id'];
    $syncSubscribeSendinblueIds[$val['type_id']] = $val['sendinblue_type_id'];
}
foreach($syncUserData as $key => $value) {
    $sandinBlue = new Sandinblue($apiKey);
    $emailAddress = $value['email'];
    $getConatct = $sandinBlue->get('contacts/' . $emailAddress);
    if(isset($getConatct['id']) && !empty($getConatct['id'])) {
        // User update case.
        $syncUserId = $updateConatctData = '';
        $syncUserId = $value['sendinblue_type_id']; // user id which already sync with SIB
        $contactDetails = $sandinBlue->setContactDetails($value, $countriesISD, $listId, $userType);
        $updateConatctData = $sandinBlue->put('contacts/'.$contactDetails['email'], $contactDetails); // Add customers with Sendinblue
        if(!empty($updateConatctData['code'])) {
            $errCust ++;
        }
        $objSendinblueSyncLogData->res_data = json_encode($updateConatctData);
        $objSendinblueSyncLogData->sync_action = 'U';
    } else {
        // User Add case.
        $syncUserId = $addConatctData = '';
        $contactDetails = $sandinBlue->setContactDetails($value, $countriesISD, $listId, $userType);
        $addConatctData = $sandinBlue->post('contacts', $contactDetails); // Add customers with Sendinblue
        if(isset($addConatctData['id'])) {
            $syncUserId = $sendinBlueUserId = $addConatctData['id'];
        }
        // Make an entry for sync data of customer
        if(!empty($sendinBlueUserId)) {
            $objSendinblueData->type_id = $value['userid'];
            $objSendinblueData->sendinblue_type_id = $sendinBlueUserId;
            $objSendinblueData->type = ($userType == 'webuser') ? 'C': 'S';
            $customerSyncId = $objSendinblueMaster->addSendinblueSyncData($objSendinblueData);
            $syncCustomerIds[] = $value['userid'];
            $syncCustomerSendinblueIds[$value['userid']] = $sendinBlueUserId;
        }
        $objSendinblueSyncLogData->res_data = json_encode($addConatctData);
        $objSendinblueSyncLogData->sync_action = 'A'; 
    }
    // // Add Request & Response of customer in log table
    $objSendinblueSyncLogData->sync_data_id = $syncUserId;
    $objSendinblueSyncLogData->data_id = $value['userid'];
    $objSendinblueSyncLogData->req_data = json_encode($sandinBlue->last_response['httpHeaders']);
    $objSendinblueSyncLogData->sync_type = ($cronAction == true) ? 'A' : 'M';
    $objSendinblueSyncLogData->sendinblue_sync_type = ($userType == 'webuser') ? 'C' : 'S';
    $contactSyncData = $objSendinblueSyncLogMaster->addSendinblueSyncLog($objSendinblueSyncLogData);
    
} 
if($limit && $userType == 'newsletter') { 
    if($unserializedData['sibSyncFrom'] != 0) {
       $limit = $unserializedData['sibSyncFrom'] + $limit;
    }
    $unserializedData['sibSyncFrom'] = $limit;
} else if($limit && $userType == 'webuser') {
    if($unserializedData['webSyncFrom'] != 0) {
        $limit = $unserializedData['webSyncFrom'] + $limit;
     }
    $unserializedData['webSyncFrom'] = $limit;
}
$unserializedData = serialize($unserializedData);
$objExternalServiceData->service_details = $unserializedData;
$objExternalServiceData->service_id = $serviceId;

$objExternalServiceMaster->editExternalService($objExternalServiceData);

if($userType == 'webuser') {
    echo 'Website User Sync Successfully.';
} else {
    echo 'Subscriber Sync Successfully.';
}
?>