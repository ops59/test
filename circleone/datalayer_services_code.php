<?php 
    require_once("lib/common.php");
    
    $order_id=$_REQUEST['OrderNo'];
    
    function get_order_payment_detailss($order_id){
        require_once(DIR_WS_MODEL . 'PaymentDetailsMaster.php');
        $PaymentDetailsMasterObj   	= new PaymentDetailsMaster();
    
        $fieldArr = array();
        $fieldArr = array('*');
        $PaymentDetailsMasterObj->setSelect($fieldArr);
        $PaymentDetailsMasterObj->setWhere("AND orders_id = :orders_id",$order_id,'string');
        $order_amount = $PaymentDetailsMasterObj->getPaymentDetails();
        $order_amount = $order_amount[0];
        $tax = unserialize($order_amount['city_tax_amount']);
        $total_tax = 0;
        if(!empty($tax)){
            foreach ($tax as $taxkey => $taxvalue){
                $total_tax += $taxvalue['tax_amount'];
            }
        }
        $order_amount['city_tax_amount'] = $total_tax;
        if($order_amount['coupon_amount'] != '0'){
            $order_amount['coupon_amount'] = $order_amount['coupon_amount'];
        }else{
            $order_amount['coupon_amount'] = '0';
        }
    
        return $order_amount;
    }
    
    function get_order_detailss($order_id){
         
        require_once(DIR_WS_MODEL . 'OrderMaster.php');
        $ObjOrderMaster				= new OrderMaster();
    
        $fieldArrfororder = array();
        $fieldArrfororder = array('*');
        $ObjOrderMaster->setSelect($fieldArrfororder);
        $ObjOrderMaster->setWhere("AND orders_id = :orders_id",$order_id,'string');
        $order_details = $ObjOrderMaster->getOrder();
        $order_details = $order_details[0];
    
        return $order_details;
    }
    
    function get_order_product_detailss($order_id){
    
        require_once(DIR_WS_MODEL . 'OrderProductMaster.php');
        require_once (DIR_WS_MODEL . "PaymentDetailsMaster.php");
        $ObjProductCategoryMaster	= new ProductCategoryMaster();
        $ObjProductDetailsMaster = new ProductsMaster();
        $OrderProductMasterObj  	= new OrderProductMaster();
    
        $fieldArrfororderproduct = array();
        $fieldArrfororderproduct = array('*');
        $OrderProductMasterObj->setSelect($fieldArrfororderproduct);
        $OrderProductMasterObj->setWhere("AND orders_id = :orders_id",$order_id,'string');
        $OrderProductdetails = $OrderProductMasterObj->getOrderProduct();
    
        $order_product_details=array();
        foreach ($OrderProductdetails as $key =>$productdata){
            $order_product_details[$key]['products_type_id']=$productdata['products_type_id'];
            $order_product_details[$key]['products_title']=$productdata['products_title'];
            $order_product_details[$key]['products_price']=$productdata['products_price'];
            $order_product_details[$key]['products_quantity']=$productdata['products_quantity'];
            $order_product_details[$key]['products_subtotal_price']=$productdata['products_subtotal_price'];
            $order_product_details[$key]['products_discount_price']=$productdata['products_discount_price'];
            $order_product_details[$key]['products_sku']=$productdata['products_sku'];
    
            $ObjProductDetailsMaster->setWhere("AND products_id = :products_id", $productdata['products_type_id'], 'int');
            $product_details=$ObjProductDetailsMaster->getProducts();
    
            $category_id=$product_details[0]['category_id'];
            $categorydata=$ObjProductCategoryMaster->get_product_category_details($category_id,SITE_LANGUAGE_ID);
            $order_product_details[$key]['category_name']=$categorydata[0]['category_name'];
            $order_product_details[$key]['category_id']=$categorydata[0]['category_id'];
        }
        
        return $order_product_details;
    }
    
    function get_category_id_name_list($order_id){
        
        $order_product_details = get_order_product_detailss($order_id);
        
        $categoryResult = array();
        foreach($order_product_details as $key => $subarray){
          foreach($subarray as $j => $k){
            $categoryResult[$j][] = $k;
          }
        }
        
        $catname = implode(",",$categoryResult['category_name']);
        $catid = implode(",",$categoryResult['category_id']);
        
        $catDetailss = array($catname,$catid);
       return $catDetailss;
    }
    
    function get_country_codes($userID){
        
        require_once(DIR_WS_MODEL . "CountryMaster.php");
        require_once(DIR_WS_MODEL . 'AddressMaster.php');
        $ObjAddressMaster 			= new AddressMaster();
        $CountryMasterObj   		= new CountryMaster();
        
//        $order_details = get_order_details($order_id);
        $country_code = '';
        $ObjAddressMaster->setWhere("AND from_signup=:from_signup",'1','string');
        $ObjAddressMaster->setWhere("AND user_id=:user_user_id",$userID,'int');
        $addressData = $ObjAddressMaster->getAddress();
        if(!empty($addressData)) {
            $addressData = $addressData[0];
            if(!empty($addressData['country'])) {
                $country_code_field = array();
                $country_code_field = array('countries_iso_code_2');
                $CountryMasterObj->setSelect($country_code_field);
                $CountryMasterObj->setWhere("AND countries_name = :countries_name",$addressData['country'],'string');
                $country_code = $CountryMasterObj->getCountry();
                $country_code = $country_code[0]['countries_iso_code_2'];
            }
        }        
        
        return $country_code;
    }
    
    function get_payment_method_name($order_id){
        
        $order_amount = get_order_payment_detailss($order_id);
        require_once(DIR_WS_MODEL . "PaymentMethodMaster.php");
	    $ObjPaymentMethodMaster = new PaymentMethodMaster();
        
	    $payment_id = $order_amount['payment_method'];
        $ObjPaymentMethodMaster->setWhere("AND  payment_method_id = :payment_method_id", $payment_id, 'int');
        $Paymentmethod = $ObjPaymentMethodMaster->getPaymentMethod();
       
        if (!empty($Paymentmethod)){
            $PaymentDetails = unserialize($Paymentmethod[0]['payment_details']);
        }
        return $PaymentDetails;
    }
    
    $order_amount = get_order_payment_detailss($order_id);
    $OrderDetails = get_order_detailss($order_id);
    $res_currency = unserialize($OrderDetails['user_currency_details']);
    $order_product_details = get_order_product_detailss($order_id);
    $country_code = get_country_codes($userId);
    $catDetailss = get_category_id_name_list($order_id);
    $PaymentDetails = get_payment_method_name($order_id);
    
    $OutputContent = "";
    if(defined(GOOGLESERVICE_CONVERSION_SCRIPT_CODE) && GOOGLESERVICE_CONVERSION_SCRIPT_CODE != ''){
        $data_api_code = GOOGLESERVICE_CONVERSION_SCRIPT_CODE;
        $ord_total_amount = $order_amount['total_amount'];
        $data_api_code = str_replace('{$order_total_amount}', $ord_total_amount, $data_api_code);
        $OutputContent .= $data_api_code;
    }
    
//     if(defined('GOOGLESERVICE_CONVERSION_ID') && GOOGLESERVICE_CONVERSION_ID != ''){
//         $conversion_id = GOOGLESERVICE_CONVERSION_ID;
//         $OutputContent .="<script type='text/javascript'>
    
//         var _gaq = _gaq || [];
//         _gaq.push(['_setAccount', '$conversion_id']);
//         _gaq.push(['_trackPageview']);
//         _gaq.push(['_addTrans',
//         '".$order_amount['orders_id']."',            // transaction ID - required
//             '".SITE_NAME."', // affiliation or store name
//             '".$order_amount['total_amount']."',         // total - required
//             '".$order_amount['city_tax_amount']."'   ,                   // tax
//             '".$order_amount['shipping_amount']."',      // shipping
//             '".$OrderDetails['customers_city']."',                   // city
//             '".$OrderDetails['customers_state']."',                  // state or province
//             '".$OrderDetails['customers_country']."'                     // country
//         ]);";
    
//         // add item might be called for every item in the shopping cart
//         // where your ecommerce engine loops through each item in the cart and
    
//         // prints out _addItem for each
//         foreach ($order_product_details as $key =>$Ordervalue){
//             $OutputContent.="_gaq.push(['_addItem',
//                 '".$order_amount['orders_id']."',        // transaction ID - required
//                 '".$Ordervalue['products_type_id']."',               // SKU/code - required
//                 '".addslashes($Ordervalue['products_title'])."',                 // product name
//                 '".addslashes($Ordervalue['category_name'])."',                  // category or variation
//                 '".$Ordervalue['products_price']/$Ordervalue['products_quantity']."',          // unit price - required
//                 '".$Ordervalue['products_quantity']."'               // quantity - required
//             ]);";
//         }
//         $OutputContent.=" _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
//         (function() {
//             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
//         })();
//         </script>";
//     }
    
    if(defined('GOOGLESERVICE_STORE_ID') && GOOGLESERVICE_STORE_ID != ''){
        $item_detail = '';
        foreach ($order_product_details as $fieldkey => $fieldvalue){
            $item_detail .= ' <span class="gts-item">
    	    <span class="gts-i-name">'.$fieldvalue['products_title'].'</span>
    	    <span class="gts-i-price">'.$fieldvalue['products_price'].'</span>
    	    <span class="gts-i-quantity">'.$fieldvalue['products_quantity'].'</span>
    	    <span class="gts-i-prodsearch-id">'.$fieldvalue['products_type_id'].'</span>
    	  </span>';
            if($fieldvalue['reference_order_id']){
                $reorder = 'Y';
            }
        }
        if(empty($reorder)){
            $reorder = 'N';
        }
        $date = strtotime("+7 day");
        $date = date('Y-m-d', $date);
        
        $OutputContent.= '<div id="gts-order" style="display:none;" translate="no">
    	  <!-- start order and merchant information -->
    	  <span id="gts-o-id">'.$order_amount['orders_id'].'</span>
    	  <span id="gts-o-domain">'.$_SERVER['SERVER_NAME'].'</span>
    	  <span id="gts-o-email">'.$order_details['customers_email_address'].'</span>
    	  <span id="gts-o-country">'.$country_code.'</span>
    	  <span id="gts-o-currency">'.$OrderDetails['currency_type'].'</span>
    	  <span id="gts-o-total">'.number_format((float)$order_amount['total_amount'], 2, '.', '').'</span>
    	  <span id="gts-o-discounts">'.number_format((float)$order_amount['coupon_amount'], 2, '.', '').'</span>
    	  <span id="gts-o-shipping-total">'.number_format((float)$order_amount['shipping_amount'], 2, '.', '').'</span>
    	  <span id="gts-o-tax-total">'.number_format((float)$order_amount['city_tax_amount'], 2, '.', '').'</span>
    	  <span id="gts-o-est-ship-date">'.$date.'</span>
      	  <span id="gts-o-est-delivery-date">'.$date.'</span>
    	  <span id="gts-o-has-preorder">'.$reorder.'</span>
    	  <span id="gts-o-has-digital">N</span>
    	  <!-- end order and merchant information -->
        
    	  <!-- start repeated item specific information -->
    	  <!-- item example: this area repeated for each item in the order -->
    	 '.$item_detail.'
    	  <!-- end repeated item specific information -->
    	  </div>';
    } 

    if(defined('GOOGLETAGMANAGER_GTM_EVENT') && GOOGLETAGMANAGER_GTM_EVENT != ''){
        // Function to return the JavaScript representation of a TransactionData/Purchase object.
        function getTransaction($OrderDetails, $order_amount, $order_product_details,$catDetailss,$PaymentDetails) {
            $order_id = $OrderDetails['order_id_type'];
            $affiliation = SITE_URL;
            $total_amount = number_format($order_amount['total_amount'], 2, '.', '');
            $total_tax_amount = number_format($order_amount['city_tax_amount'], 2, '.', '');
            $shipping_amount = number_format($order_amount['shipping_amount'], 2, '.', '');
            $payment_date = $order_amount['payment_date'];
            $coupon_code = $order_amount['coupon_code'];
            $customerID = $OrderDetails['user_id'];
            $dicountamount = $order_amount['coupon_amount'];
            $categoryList = $catDetailss[0];
            $categorylistID = $catDetailss[1];
            $mcrypt = new ops_encryption_string();
            $customeremail = $mcrypt->encrypt($OrderDetails['customers_email_address']);
            $PaymentMethod = $PaymentDetails['payment_title'][1];
            $datalayer .="<script>
       dataLayer.push({ 
	     'event' : 'purchase',
           'ecommerce': {
             'purchase': {
                'actionField': {
                'id': '$order_id',
                'revenue': '$total_amount',
                'tax':'$total_tax_amount',
                'shipping': '$shipping_amount',
                'coupon':'$coupon_code',
                'categoryList': '$categoryList',
                'categoryId': '$categorylistID',
                'customerId': '$customerID',
                'customerStatus': '1',
                'discountAmount': '$dicountamount',
                'encryptcustomerEmail': '$customeremail',
                'payment_gateway':'$PaymentMethod'
        },
        'products':[";
            $i=1;
            foreach ($order_product_details as $fieldkey => $fieldvalue) {
                $title = $fieldvalue['products_title'];
                $id = $fieldvalue['products_type_id'];
                $price = $fieldvalue['products_subtotal_price']/$fieldvalue['products_quantity'];
                $productprice = number_format($price, 2, '.', '');
                $category = $fieldvalue['category_name'];
                $categoryID = $fieldvalue['category_id'];
                $quantity = $fieldvalue['products_quantity'];
                $total_product_price = $fieldvalue['products_subtotal_price'];
                $productsku = $fieldvalue['products_sku'];
                $productdiscount_amount = $fieldvalue['products_discount_price'];
                $productdiscount_unit_amount = $fieldvalue['products_discount_price']/$fieldvalue['products_quantity'];
                if($i == '1'){
                    $datalayer .="{";
                }
                else {
                    $datalayer .="
                {";
                }
                $datalayer .="
                'name': '$title',
                'id': '$id',
                'price': '$productprice',
                'brand': 'CircleOne',
                'category': '$category',
                'variant': '',
                'quantity': '$quantity',
                'categoryId': '$categoryID',
                'discount_amount': '$productdiscount_amount',
                'discounted_unit_price': '$productdiscount_unit_amount',
                'sku': '$productsku',
                'product_total_Price': '$total_product_price'   
                }";
                if($i != count($order_product_details)){
                    $datalayer .= ",";
                }
                $i++;
            }
            $datalayer.="]
        }
       }
      });
    </script>";
            return $datalayer;
        }
        
        $OutputContent .= getTransaction($OrderDetails, $order_amount, $order_product_details,$catDetailss,$PaymentDetails);
    }
    //We added this now in our standard code so no need to define it here seprately
    //facebook pixel purchase event track
    // if(defined('FACEBOOKPIXEL_FB_PIXEL_EVENT') && FACEBOOKPIXEL_FB_PIXEL_EVENT != ''){
    //     $data_fb_pixel_event = explode(",",FACEBOOKPIXEL_FB_PIXEL_EVENT);
    //     if(!empty($data_fb_pixel_event)){
    //         foreach ($data_fb_pixel_event as $k=>$v){
    //             $fb['FBP_'.$v] = $v;
    //         }
    //     }
    //     if ($fb['FBP_2'] != 'undefined' || $fb['FBP_2'] !=''){
    //         $total =  $order_amount['total_amount'];
    //         $currency = $res_currency['currency_code'];
    //         $OutputContent .="<script>
    //         fbq('track', 'Purchase',{
    //         value: '$total',
    //         currency: '$currency',
    //         orderid:'$order_id'
    //     });
    //     </script>";
    //     }
    // }
    
    $OUTPUT = $OutputContent;
    return $OUTPUT;
   
?>