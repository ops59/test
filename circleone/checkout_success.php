<?php
	/**
	* This file is load the card and Display card
	*
	* This file shows the card lising. we can select card from this. This is also apply search criteria.This file load studio & Display the card in studio
	*
	*
	* @author     Radixweb <team.radixweb@gmail.com>
	* @copyright  Copyright (c) 2008, Radixweb
	* @version    1.0
	* @since      1.0
	*/
	require_once("lib/common.php");
	require_once(DIR_WS_MODEL."BasketMaster.php");
	
	require_once (DIR_WS_MODEL . "PaymentMethodMaster.php");
	require_once (DIR_WS_PAYMENT . "payment.php");
	require_once (DIR_WS_MODEL . "OrdersMasters.php");
	require_once(DIR_WS_MODEL."CmsPagesMaster.php");
	require_once(DIR_WS_MODEL."ProcessStatusMaster.php");
	$BasketMasterObj 	= new BasketMaster();
    $ObjOrderMaster = new OrderMaster();
    $ObjProductCategoryMaster = new ProductCategoryMaster();
    $ObjProductDetailsMaster = new ProductsMaster();
    $ObjPaymentDetailsMaster = new PaymentDetailsMaster();
    $ObjPaymentMethodMaster = new PaymentMethodMaster();
    $ObjCmsPagesMaster	= new CmsPagesMaster();
    $ObjProcessStatusMaster = new ProcessStatusMaster();
	
	
	$ObjProductCategoryMaster	= new ProductCategoryMaster();
	$ObjProductDetailsMaster	= new ProductsMaster();
	//Clear Session & Emplty Cart while Order has been placed
        $sesUserId = $RRequest->get('sesUserId');
	if($sesUserId !=''){
		$userId = $sesUserId;
    } elseif (!empty($RAccess->getUserID())) {
        $userId = $RAccess->getUserID();
	} else {
	    $userId='';
	}
	/**
	 * user login check
	 */		
	if($userId==''){
		show_page_header(FILE_INDEX);
	}
	 
    $OrderNo   = $RRequest->get("OrderNo","int");
    $add       = $RRequest->request("add");	
	require_once (DIR_WS_MODEL . "PaymentDetailsMaster.php");
	if(isset($add)) {
	    
		require_once(DIR_WS_MODEL."CorporateUserMaster.php");
		require_once(DIR_WS_MODEL . 'OrderMaster.php');
		require_once(DIR_WS_LIB.'cart_class.php');
		
		$CorporateMasterObj = new CorporateUserMaster();
		$ObjOrderMaster		= new OrderMaster();
		$OrderDataObj	  	= new OrderData();
		$objShoppingCart    = new Shopping_Cart_Data(true,true,true,true,true);
   		$basket_arr 		= $objShoppingCart->cart_details;
   		 
		if(!empty($RAccess->getUserType()) && $RAccess->getUserType() != '1') {
	    	if(strtolower(CORPORATE_MODUEL_ENABLE) == 'yes') {    
		    	$corporateId = $RAccess->getStoreID();    
		    }
   		}
   		
   		// Start Reward Points Calculation
   		if (!empty($RAccess->getUserType()) && $RAccess->getUserType() == '1' && SES_CUSTOMER_TYPE!=0) {
   			
   			$objRewardData              = new reward();
   			$objRewardData->reward_points_calculation($objShoppingCart,$basket_arr,$OrderNo);
   		}
   		// End Reward
   		
   		// code to remove upload center template
		require_once(DIR_WS_MODEL.'UsertemplateMaster.php');
		$ObjTempleteMaster  = new UsertemplateMaster();
		$cntOrder 			= count($basket_arr);
		if($cntOrder!=0) {
			for($k=0;$k<$cntOrder;$k++) {
				if(!empty($basket_arr) && ($basket_arr[$k]["product_type"] == 1 || $basket_arr[$k]["product_type"] == 8) && !empty($basket_arr[$k]["template_id"])) {
					$ObjTempleteMaster->deleteUsertemplate($basket_arr[$k]["template_id"]);
					recursive_remove_directory(DIR_WS_IMAGES_USERTEMPLATES.$basket_arr[$k]["template_id"]);
				}
				if(!empty($basket_arr) && ( $basket_arr[$k]["studio_mode"] == 6 || $basket_arr[$k]["studio_mode"] == 7)  && !empty($basket_arr[$k]["template_id"])) {
					$ObjTempleteMaster->deleteUsertemplate($basket_arr[$k]["template_id"]);
					recursive_remove_directory(DIR_WS_IMAGES_USERTEMPLATES.$basket_arr[$k]["template_id"]);
				}
				if(!empty($basket_arr) && ( $basket_arr[$k]["product_type"] == 0 && $basket_arr[$k]["predefined_product_type"] == PHOTOPRINT_PRODUCT) && !empty($basket_arr[$k]["template_id"])) {
				    $ObjTempleteMaster->deleteUsertemplate($basket_arr[$k]["template_id"]);
				    recursive_remove_directory(DIR_WS_IMAGES_USERTEMPLATES.$basket_arr[$k]["template_id"]);
				}
			}
		}
	}
	$BasketMasterObj->deleteBasket(null,$userId);
	$__Session->ClearValue("_Sess_Front_ShippingVal");
	$__Session->ClearValue("_Sess_Front_SubtotVal");
	$__Session->ClearValue("_Sess_Front_TotAmt");
	$__Session->ClearValue("_SessionOrderComments");
	$__Session->ClearValue("_SessionPayment");
	$__Session->ClearValue("Sess_Common_Nav");
	$__Session->ClearValue("_SessionPostdata");
	$__Session->ClearValue("_session_checkout_details");
	$__Session->ClearValue("thirdparty_shipping_billing_method");
	// Clear the session for cart items once checkout is done.
	$__Session->ClearValue("_Sess_Cart_Item");		
	/** if guestcustomer then clear session for user detail **/
	if(SES_CUSTOMER_TYPE==0) {
		$__Session->ClearValue("_sess_login_userdetails");
		$__Session->ClearValue("_Session_Template_Design");
		$__Session->ClearValue("_Session_Uploader_Type_Latter");
	}
	$__Session->ClearValue('Sess_shipping_details');
	$__Session->Store();
	unset($_SESSION['fb']);
	/*It has been commented as the status of the order other than success are not available as of this code*/
	/*else {
		header("Location:".FILE_INDEX);
	}*/
	
	$OrderDetails = null;
	if(!empty($OrderNo)) {
		require_once("lib/reward.php");
	    /**
		 * Check for valid order
		 */
	    $ObjOrderMaster->setWhere("AND orders_id = :orders_id", $OrderNo, 'int')
	                   ->setWhere("AND orders_date_finished >= :orders_date_finished", date('Y-m-d H:i:s',time()-3600) , 'string')
	                   ->setWhere("AND user_id = :user_id", $userId , 'int');
    	
    	$OrderDetails = $ObjOrderMaster->getOrder();
    	if(empty($OrderDetails)){
    		show_page_header(FILE_INDEX);
    	}
    	
    	if(defined('ES_LEADDYNO') && defined('LEADDYNO_PRIVATE_KEY') && LEADDYNO_PRIVATE_KEY != '') {
			require_once(DIR_WS_EXTERNALSERVICE.'leaddyno/functions.php');
		   	leaddyno_order_placed($OrderNo); // Tracks purchases from Order in LeadDyno
		}
		
		if(!empty($RAccess->getUserID()) && defined('ES_KLAVIYO') && constant("KLAVIYO_PUBLIC_KEY")!='' && strpos(constant("KLAVIYO_KLAVIYO_EVENT"),'3')){
		    require_once(DIR_WS_EXTERNALSERVICE.'klaviyo/functions.php');
		    klaviyo_placed_orders($OrderNo); // Placed Order track into KLAVIYO
		}
    	
    	//Add entry in order_export_history for hot copy option
    	require_once(DIR_WS_MODEL . 'OrderExportMaster.php');
    	$ObjOrderExportMaster = new OrderExportMaster();
    	$ObjOrderExportMaster->updateExportOrderFlag($OrderNo, true);
	    
		require_once(DIR_WS_MODEL."PaymentDetailsMaster.php");
		$objPaymentDetailsMaster = new PaymentDetailsMaster();
		
		$objPaymentDetailsMaster->setWhere("AND orders_id = :orders_id", $OrderNo, 'int');
		$objpayment_mstdetails = $objPaymentDetailsMaster->getPaymentDetails();
		$ord_total_amount = $objpayment_mstdetails[0]['total_amount'];
		$tax_amount 		= unserialize($objpayment_mstdetails[0]['city_tax_amount']);
		
		$ObjOrderMaster->setWhere("AND orders_id = :orders_id", $OrderNo, 'int');
		$OrderDetails = $ObjOrderMaster->getOrder();
		$OrderDetails = $OrderDetails[0];
		$res_currency = unserialize($OrderDetails['user_currency_details']);
		$processData = $ObjProcessStatusMaster->getProcessStatusDesc(SITE_LANGUAGE_ID, $OrderDetails['orders_status_id']);
		$displayCurrentStatus=display_order_status($processData[0]->process_status_id, $processData[0]->process_status_title, $processData[0]->process_status_complete, $OrderDetails['order_approval_status']);
		
		
		//order product
		require_once(DIR_WS_MODEL . 'OrderProductMaster.php');
		$ObjProductMaster = new OrderProductMaster(); 

		$ObjProductMaster->setWhere("AND orders_id = :orders_id", $OrderNo, 'int')
                         ->setJoin("LEFT JOIN products_description ON orders_products.products_type_id=products_description.products_id AND site_language_id = :site_language_id", SITE_LANGUAGE_ID, 'int');
		$OrderProductdetails = $ObjProductMaster->getOrderProduct();
		
		$order_product_details=array();
		foreach ($OrderProductdetails as $key =>$productdata){
			$order_product_details[$key]['products_type_id']=$productdata['products_type_id'];
			$order_product_details[$key]['products_title']=$productdata['products_title'];
			$order_product_details[$key]['products_price']=$productdata['products_price'];
			$order_product_details[$key]['products_quantity']=$productdata['products_quantity'];
			
			$ObjProductDetailsMaster->setWhere("AND products_id = :products_id", $productdata['products_type_id'], 'int');			
			$product_details=$ObjProductDetailsMaster->getProducts();
			
			$category_id=$product_details[0]['category_id'];
			$categorydata=$ObjProductCategoryMaster->get_product_category_details($category_id,SITE_LANGUAGE_ID);
			$order_product_details[$key]['category_name']=$categorydata[0]['category_name'];
		}
	}
	
	$output_content = "";
	if($add =="success") {
		if(in_array($objpayment_mstdetails[0]->payment_method ,$payonMethodIDs)) {
//			if($corporateId != '' && $corporateId != 0){
//				$corporate_seaArr =array();
//				$fieldArr	= array();
//				$fieldArr = array('order_approval');
//				$corporate_seaArr[]	= array('Search_On'=>'corporate_id','Search_Value'=>$corporateId,'Type'=>'int','Equation'=>'=','CondType'=>'AND','Prefix'=>'','Postfix'=>'');
//					
//				$getCorporateData = $CorporateMasterObj->getCorporateUser($fieldArr,$corporate_seaArr);
//				$getCorporateData = $getCorporateData[0];
//			}
			//if(!empty($getCorporateData)){
				if($OrderDetails['order_approval_status'] == 1){
					$output_content .= ADMIN_CONFIRMATION_MESSAGE_FOR_ORDER_WITH_PAYON;
				}
			//}
		} else {
		  $orderno=$OrderDetails['order_id_type'];
		  $output_content .= sprintf(CLICK_HERE_TO_VIEW_DETAILS,'<a class="action_link" href="' .show_page_link(FILE_ORDERS_DETAILS.'?OrderId='.$OrderNo,false) .'" >' . $orderno . '</a>','<a class="action_link" href="' .show_page_link(FILE_ORDERS_DETAILS.'?OrderId='.$OrderNo,false) .'" >' . CLICK_HERE . '</a>');
		}
		if($objpayment_mstdetails[0]['transactionid'] !='') {
			$output_content .= '<br>'.sprintf(TRANSACTION_ID,$objpayment_mstdetails[0]['transactionid']);
		}
		if (ENABLE_REWARD_MODULE =='yes') {
			require_once("lib/reward.php");
			$objReward             	 	= new reward();
			$ObjRewardHistorydata       = $objReward->ObjRewarHistoryData;
			$ObjRewardHistoryMaster     = $objReward->ObjRewardHistoryMaster;

			$ObjRewardHistoryMaster->setSelect("(SELECT Sum(reward_history.reward_points) FROM reward_history WHERE (reward_history.user_id = :user_id AND reward_history.order_id = :order_id AND ( reward_history.reward_transaction_type = 'EO' ) )) as Earn", 
                                        array(":user_id" => $userID, ":order_id" => $OrderDetails['orders_id']),
			                             array(":user_id" => 'int', ":order_id" => 'int'))
            ->setSelect("(SELECT Sum(reward_history.reward_points) FROM   reward_history WHERE  ( reward_history.user_id = :user_id_1  AND  reward_history.reward_transaction_type = :reward_transaction_type_sc  )) as Spent", array(
            ":user_id_1" => $userID,
            ":reward_transaction_type_sc" => "SC"
        ), array(
            ":user_id_1" => "int",
            ":reward_transaction_type_sc" => "string"
        ))
            ->setWhere("AND reward_history.user_id = :reward_history_user_id", $userID, 'int')
            ->setWhere("AND reward_history.order_id = :reward_history_order_id", $OrderDetails['orders_id'], 'int');
        
        $RewardHistoryList = $ObjRewardHistoryMaster->getRewardHistory();
     
        if ($RewardHistoryList[0] != '') {
            if ($RewardHistoryList[0]['Earn'] != '' && $RewardHistoryList[0]['Earn'] > 0) {
                $total_earned_reward_points = $RewardHistoryList[0]['Earn'];
                $output_content .= '<br><br>' . sprintf(CHECKOUT_REWARD_CONTENT, $total_earned_reward_points, '<a class="action_link" href="' . show_page_link(FILE_USER_REWARD, false) . '" >' . CLICK_HERE . '</a>');
            }
            $total_previous_points = $RewardHistoryList[0]['Earn1'];
        }
        
        $ObjRewardHistoryMaster->setSelect("(SELECT Sum(reward_history.reward_points) FROM reward_history WHERE ( reward_history.user_id = :user_id AND ( reward_history.reward_transaction_type = 'ER' OR reward_history.reward_transaction_type = 'EO' ) )) as Earn", array(":user_id" => $userID, ":reward_transaction_type_er" => "ER", ":reward_transaction_type_eo" => "EO" ), array(":user_id" => "int", ":reward_transaction_type_er" => "string", ":reward_transaction_type_eo" => "string" ))
        ->setSelect("(SELECT Sum(reward_history.reward_points) FROM reward_history WHERE ( reward_history.user_id = :user_id_1 AND  reward_history.reward_transaction_type = :reward_transaction_type_sc )) as Spent", array(":user_id_1" => $userID, ":reward_transaction_type_sc" => "SC"), array(":user_id_1" => "int", ":reward_transaction_type_sc" => "string"))
        ->setSelect("(SELECT Sum(reward_history.reward_points) FROM reward_history WHERE ( reward_history.user_id = :user_id_2 AND  reward_history.reward_transaction_type = :reward_transaction_type_soc )) as Canceled", array(":user_id_2" => $userID, ":reward_transaction_type_soc" => "SOC"), array(":user_id_2" => "int", ":reward_transaction_type_soc" => "string"))
        ->setWhere("AND reward_history.user_id = :reward_history_user_id", $userID, 'int')
        ->setLimit(0,1);
        $RewardHistoryList1  = $ObjRewardHistoryMaster->getRewardHistory();
       $RewardHistoryList1  = $RewardHistoryList1[0];
       $total_all_points = $RewardHistoryList1['Earn'];
       $spent_points=$RewardHistoryList1['Spent'];
       $Canceled_points=$RewardHistoryList1['Canceled'];
        $total_points = ($total_all_points - $spent_points-$Canceled_points);
        $total_previous_points=$total_points-$total_earned_reward_points;
 				}
		if($printland_api_code != '') {
			$output_content .= $printland_api_code;
		}
		// :: Start : Add order to Mailchimp E-Commerce Store
		if ((defined('ES_MAILCHIMPAUTOMATION') && constant('ES_MAILCHIMPAUTOMATION') == true && MAILCHIMPAUTOMATION_ORDER_NOTIFICATION_AUTOMATION == 1)) {
			require_once (DIR_WS_EXTERNALSERVICE . 'mailchimpautomation/functions.php');
			if (MAILCHIMPAUTOMATION_CART_NOTIFICATION_AUTOMATION == 1) {
				foreach ($OrderProductdetails as $OrderProductdetail) {
					Mailchimpautomation::addEditProduct($OrderProductdetail);
					$Productdetail[] = $OrderProductdetail;
				}
			}
			$objShoppingCart->order_id = $OrderDetails['orders_id'];
			$objShoppingCart->customer_id = $RAccess->getUserID();
			$objShoppingCart->paymentdetails = $objpayment_mstdetails[0];
			$objShoppingCart->orders_date_finished = $OrderDetails['orders_date_finished'];
			$objShoppingCart->cart_details = $Productdetail;
			Mailchimpautomation::addEditOrder($objShoppingCart);
		}
		// :: End : Add order to Mailchimp E-Commerce Store
			
		/**
    	 *  Yotpo External Service - Mail after purchase
    	 */
    	if(defined('ES_YOTPO') && strtolower(YOTPO_MAIL_AFTER_PURCHASE)=='yes'){
    		require_once(DIR_WS_EXTERNALSERVICE."yotpo/yotpo.class.php");
    		$yotpo_app_key = YOTPO_APP_KEY;
    		$yotpo_secret = YOTPO_SECRET;
    		if(!empty($yotpo_app_key) && !empty($yotpo_secret)){
    			try {
    				$purchase_data['order_id'] = $OrderNo;
    				$purchase_data['order_date'] = $OrderDetails['orders_date_finished'];
    				$purchase_data['email'] = $OrderDetails['customers_email_address'];
    				$purchase_data['customer_name'] = $OrderDetails['customers_name'];
    				$purchase_data['platform'] = "general";
    				$purchase_data['currency_iso'] = COMMON_CURRENCY_SYMBOL;
    				
					/*product array*/
					foreach ($OrderProductdetails as $OrderProductdetail) 
					{
						$product_data = array();   
						$product_data['url'] = show_page_link(FILE_PRODUCT_DESCRIPTION).'?pid='.$OrderProductdetail['products_type_id'];
						$product_data['name'] = $OrderProductdetail['products_title'];
						$product_data['image'] = DIR_HTTP_IMAGES_PRODUCT.$OrderProductdetail['imagename'];
						$product_data['description'] = $OrderProductdetail['product_description'];
						$product_data['price'] = currency_format($OrderProductdetail['products_price']);
						$products_arr[$OrderProductdetail['products_type_id']] = $product_data;	
					}
					/*end of product array*/
					$purchase_data['products'] = $products_arr;
					$yotpo_api = new Yotpo(YOTPO_APP_KEY, YOTPO_SECRET);
					$get_oauth_token_response = $yotpo_api->get_oauth_token();
					if(!empty($get_oauth_token_response) && !empty($get_oauth_token_response['access_token'])) {
						$purchase_data['utoken'] = $get_oauth_token_response['access_token'];
						$response = $yotpo_api->create_purchase($purchase_data);
					}
    			}
    			catch (Exception $e) {
    				error_log($e->getMessage());
    			}
    		}
    	}
    	
    	/**
    	  * AddShoppers
    	  */
    	if(defined('ES_ADDSHOPPERS')){
    	    require_once(DIR_WS_EXTERNALSERVICE."addshoppers/addshoppers.class.php");
	        $addshoppers_api = new Addshoppers(ADDSHOPPERS_APP_KEY, ADDSHOPPERS_SECRET);
	        //ROI Tracking
    	    if(strtolower(ADDSHOPPERS_ROI_TRACKING)=='yes' && ADDSHOPPERS_SHOP_ID!='') {
    	        $twd['addshoppers_roi_tracking'] = $addshoppers_api->show_roi_tracking(ADDSHOPPERS_SHOP_ID, $OrderNo, $ord_total_amount);
    	    }
    	    
    	    //Purchase Sharing
    	    if(strtolower(ADDSHOPPERS_PURCHASE_SHARING)=='yes'){
    	        if(count($OrderProductdetails)==1){
    	            $OrdPrddetail = $OrderProductdetails[0];
					$header=ADDSHOPPERS_PURCHASE_SHARING_HEADER;
					$image = DIR_HTTP_IMAGES_PRODUCT.$OrdPrddetail['imagename'];
					$link = show_page_link(FILE_PRODUCT_DESCRIPTION).'?pid='.$OrdPrddetail['products_type_id'];
					$title = $OrdPrddetail['products_title'];
					$price = currency_format($OrdPrddetail['products_price']);
					$content = $OrdPrddetail['product_description'];
					$cont = nl2br($content);
					$content = preg_replace("#(<br\s*/?>\s*){2,}#"," ",$cont);
					$content= trim(htmlentities(strip_tags($content)));
					
    	        } else {
					$header=ADDSHOPPERS_PURCHASE_SHARING_HEADER;
    	            $image = sprintf(SITE_LOGO_PATH,SITE_TEMPLATE);
					$link = ADDSHOPPERS_PURCHASE_SHARING_LINK;
					$title = ADDSHOPPERS_PURCHASE_SHARING_TEXT;
					$price = '';
					$content =ADDSHOPPERS_PURCHASE_SHARING_DESC;
    	        }
    	        $twd['addshoppers_purchase_sharing'] = $addshoppers_api->show_addshoppers_purchase_sharing($header, $image, $link, $title, $price, $content);
    	    }
    	}
	}
	
	$outputcontent = "";
	$orderId=$RRequest->request("OrderNo","int");
	if(isset($add) && $add=="success") {
	   $outputcontent = include(DIR_WS_LOCALCONFIG_LIB.'google_services_code.php');
	}
	
	if(!empty($orderId)){
	$ObjPaymentDetailsMaster->setWhere("AND Orders_Id = :Orders_Id", $orderId, 'int');
	$payment_details = $ObjPaymentDetailsMaster->getPaymentDetails();
	$OrderPayment = $payment_details[0];
	// This is for the get payment method.
	$PaymentMethodsArr = getpaymentMethod($OrderPayment['payment_method']);
	
	$ObjOrderMaster->setWhere("AND orders.orders_id = :orders_orders_id", $orderId, 'int')
	->setSelect(array(
	    'orders.*',
	    'payment_details.total_amount'
	))
	->setJoin("LEFT JOIN payment_details ON payment_details.orders_id=orders.orders_id");
	$resO = $ObjOrderMaster->getOrder();
	$OrderDetails = $resO[0];
	}
	
	$dashboard_array = array();
	$dashboard_array[] =  array('file' => FILE_ORDER, 'title' => HEADING_USER_ORDER, 'description' => USER_ORDER_TAG_LINE,  'icon' => '<i class="cicon  cicon-orders2 mr-2 fa-3x"></i>');
	if(defined('WITHOUT_DESIGNER_SUBSCRIPTION') && WITHOUT_DESIGNER_SUBSCRIPTION == 'No'){
	    if (($keys = array_search(FILE_PORTFOLIO,$arr_myaccount_menu_link )) !== false) {
	       $dashboard_array[] =  array('file' => FILE_PORTFOLIO, 'title' => HEADING_USER_PORTFOLIO, 'description' => USER_PORTFOLIO_TAG_LINE,  'icon' => '<i class="cicon  cicon-portfolio2 mr-2 fa-3x"></i>');
	    }
	}
	$display_quote = true;
    if ($RAccess->isDeptManager() && (defined('SES_CORPORATE_DEPARTMENT_MANAGER_ALLOW_CHECKOUT') && !SES_CORPORATE_DEPARTMENT_MANAGER_ALLOW_CHECKOUT ))
	    $display_quote = false;
	if(DISPLAY_QUOTE_MODULE=='yes' && $display_quote == true )
	   	$dashboard_array[] =  array('file' => FILE_QUOTE_HOME, 'title' =>HEADING_QUOTE, 'description' => QUOTE_HOME_TAG_LINE,  'icon' => '<i class="cicon  cicon-quotes mr-2 fa-3x"></i>');
	
	   	
    if($RAccess->isStore()) {
		foreach($dashboard_array as $dashboard_keys => $dashboard_link){
			if (($keys = array_search($dashboard_link['file'],$arr_myaccount_menu_link)) === false) {
				unset($dashboard_array[$dashboard_keys]);
			}
		}
		if(defined('CORPORATE_MYACCOUNT_LINK') && strtolower(CORPORATE_MYACCOUNT_LINK) == 'none' ) {
			unset($dashboard_array);
		}
	}
	
	$output_content = "";
	$orderId=$RRequest->request("OrderNo","int");
	if(isset($add) && $add=="success") {
	    $output_content = include('datalayer_services_code.php');
	}
	
	$twd['dashboard_array'] = $dashboard_array;
	$twd['output_content'] = $output_content;
	$twd['successMsg'] = $successMsg;
	$twd['orderid'] = $OrderNo;
	$twd['transactionid'] = $OrderPayment['transactionid'];
	$twd['payment_date'] = $OrderPayment['payment_date'];
	$twd['PaymentMethodsArr'] = $PaymentMethodsArr;
	$twd['resShip'] = $resO;
	$twd['total_earned_points'] = $total_earned_reward_points;
	$twd['total_previous_points'] = $total_previous_points;
	$twd['total_points'] = $total_points;
	$twd['displayCurrentStatus'] = $displayCurrentStatus;
	$twd['order_id_type'] = $OrderDetails['order_id_type'];
	$twd['outputcontent'] = $outputcontent;
	$twd['order_amount'] = $OrderPayment['order_amount'];
    $twd['currency_code'] = $OrderDetails['currency_type'];
	echo $twig->render('success.tpl', array('twd' => $twd));
?>